import UIKit
import MapKit
import CoreLocation

class FirstViewController: UIViewController, CLLocationManagerDelegate {
    
    let mapView = MKMapView()
    let leftMargin:CGFloat = 10
    let botMargin:CGFloat = -100
    let mapHeight:CGFloat = 300
    private let locationManager = CLLocationManager()
    var latitude: Double?
    var longitude: Double?
    var offerModel: MainWeatherModel?
    var weatherArray: [SaveWeatherModel] = []
    
    lazy var weatherLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 35, weight: .light)
        label.textAlignment = .left
        return label
    }()
    
    lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 25, weight: .light)
        label.textAlignment = .left
        return label
    }()
    
    lazy var cityLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 40, weight: .light)
        label.textAlignment = .left
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .purple
        self.nextStep {
            guard let element = UserDefaults.standard.value([SaveWeatherModel].self, forKey: "Weather") else {return}
            self.weatherArray = element
        }
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        configureMapView()
        configureWeatherLabel()
        configureDescriptionLabel()
        configureCityLabel()
    }
    
    func nextStep(complition: @escaping () -> ()) {
        if !UserDefaults.isFirstLaunch() {
            UserDefaults.setFirstLaunch(true)
        } else {
            complition()
        }
    }
    
    private func configureMapView() {
        let initalLocation = CLLocation(latitude: latitude ?? 53.856173, longitude: longitude ?? 27.519185)
        mapView.centerToLocation(initalLocation)
        let point = MKPointAnnotation()
        point.coordinate = initalLocation.coordinate
        point.title = "Hello"
        point.subtitle = "You are here"
        mapView.addAnnotation(point)
        
        mapView.mapType = MKMapType.standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        mapView.center = view.center
        view.addSubview(mapView)
        mapView.translatesAutoresizingMaskIntoConstraints = false
        mapView.widthAnchor.constraint(equalToConstant: view.frame.size.width-20).isActive = true
        mapView.heightAnchor.constraint(equalToConstant: mapHeight).isActive = true
        mapView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leftMargin).isActive = true
        mapView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: botMargin).isActive = true
        mapView.layer.cornerRadius = 15
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(revealRegionDetails(sender:)))
        mapView.addGestureRecognizer(recognizer)
        
    }
    
    @objc func revealRegionDetails(sender: UITapGestureRecognizer) {
        let touchLocation = sender.location(in: mapView)
        let locationCoordinate = mapView.convert(touchLocation, toCoordinateFrom: mapView)
        print("Tapped at lat: \(locationCoordinate.latitude) long: \(locationCoordinate.longitude)")
        
        NetworkManager.shared.sendRequest(coordinates: locationCoordinate) { (model) in
            let point = MKPointAnnotation()
            point.coordinate = locationCoordinate
            guard let temp = model?.current?.temp else {return}
            point.title = "\(Int(temp))"
            self.mapView.addAnnotation(point)
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager,
                         didUpdateLocations locations: [CLLocation]) {
        let location: CLLocationCoordinate2D = manager.location!.coordinate
        self.latitude = location.latitude
        self.longitude = location.longitude
        setupWeather()
    }
    
    private func setupWeather() {
        NetworkManager.shared.getWeather(latitude: self.latitude ?? 53.856173, longitude: self.longitude ?? 27.519185, completion:  {  model in
            self.offerModel = model
            guard let city = model?.timezone,
                  let date = model?.current?.dt,
                  let description = model?.current?.weather?.first?.description,
                  let lat = model?.lat,
                  let lon = model?.lon,
                  let temperature = model?.current?.temp else {return}
            self.cityLabel.text = "\(city)"
            self.descriptionLabel.text = "\(description)".firstUppercased
            self.weatherLabel.text = "\(Int(temperature))º"
            let weatherModel = SaveWeatherModel(date: date, city: city, longitude: lon, latitude: lat, temperature: temperature)
            var element = self.weatherArray
                element.append(weatherModel)
            UserDefaults.standard.set(encodable: element, forKey: "Weather")
            UserDefaults.standard.synchronize()
        })
    }
    
    private func configureWeatherLabel() {
        view.addSubview(weatherLabel)
        weatherLabel.translatesAutoresizingMaskIntoConstraints = false
        weatherLabel.widthAnchor.constraint(equalToConstant: view.frame.size.width-20).isActive = true
        weatherLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        weatherLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leftMargin).isActive = true
        weatherLabel.bottomAnchor.constraint(equalTo: mapView.topAnchor, constant: -20).isActive = true
    }
    
    private func configureDescriptionLabel() {
        view.addSubview(descriptionLabel)
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.widthAnchor.constraint(equalToConstant: view.frame.size.width-20).isActive = true
        descriptionLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        descriptionLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leftMargin).isActive = true
        descriptionLabel.bottomAnchor.constraint(equalTo: weatherLabel.topAnchor, constant: -20).isActive = true
    }
    
    private func configureCityLabel() {
        view.addSubview(cityLabel)
        cityLabel.translatesAutoresizingMaskIntoConstraints = false
        cityLabel.widthAnchor.constraint(equalToConstant: view.frame.size.width-20).isActive = true
        cityLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        cityLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leftMargin).isActive = true
        cityLabel.bottomAnchor.constraint(equalTo: descriptionLabel.topAnchor, constant: -20).isActive = true
    }
    
}

private extension MKMapView {
    func centerToLocation(
        _ location: CLLocation,
        regionRadius: CLLocationDistance = 25000
    ) {
        let coordinateRegion = MKCoordinateRegion(
            center: location.coordinate,
            latitudinalMeters: regionRadius,
            longitudinalMeters: regionRadius)
        setRegion(coordinateRegion, animated: true)
    }
}
