import Foundation


extension UserDefaults {
    
    static func isFirstLaunch () -> Bool {
        standard.bool(forKey: "firstLaunch")
    }
    
    static func setFirstLaunch(_ value: Bool) {
        standard.setValue(value, forKey: "firstLaunch")
    }
}
