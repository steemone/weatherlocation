import UIKit

class MainWeatherModel: Codable {
    var lat: Double?
    var lon: Double?
    var timezone: String?
    var timezone_offset: Int?
    var current: CurrentWeatherModel?
    var daily: [DailyWeatherModel]?
    var hourly: [HourlyWeatherModel]?
}
