import UIKit

class WeatherModel: Codable {
    var id: Int?
    var main : String?
    var description: String?
    var icon: String?
}

