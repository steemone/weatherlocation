import UIKit

class LocationCell: UITableViewCell {
    
    lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont(name: "Oswald", size: 20)
        label.font = UIFont.systemFont(ofSize: 20, weight: .light)
        label.textAlignment = .left
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .purple
        configureInfoLabelConstaint()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureInfoLabelConstaint() {
        contentView.addSubview(infoLabel)
        infoLabel.translatesAutoresizingMaskIntoConstraints = false
        infoLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10).isActive = true
        infoLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 0).isActive = true
        infoLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0).isActive = true
        infoLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0).isActive = true
    }
    
    func configureSetupInfoLabel(model: SaveWeatherModel) {
        contentView.addSubview(infoLabel)
        let date = model.date,
              location = model.city,
              lon = model.longitude,
              lat = model.latitude
        let currentDate = Date(milliseconds: date * 1000)
        self.infoLabel.text = "\(dateFormatter(date: currentDate)), location: \(location) : \(Int(lat))/\(Int(lon))"
    }
    
    func dateFormatter(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM, HH:mm"
        return dateFormatter.string(from: date)
    }

}
