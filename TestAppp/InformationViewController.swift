import UIKit

class InformationViewController: UIViewController {
    
    var saveModel: SaveWeatherModel
    
    lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont(name: "Oswald", size: 50)
        label.font = UIFont.systemFont(ofSize: 50, weight: .light)
        label.textAlignment = .center
        return label
    }()
    
    init(saveModel: SaveWeatherModel) {
        self.saveModel = saveModel
        super.init(nibName: nil, bundle: nil)
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .purple
        configureInfoLabelConstaint()
        configureSetupInfoLabel(model: saveModel)
    }
    
    func configureSetupInfoLabel(model: SaveWeatherModel) {
        self.view.addSubview(infoLabel)
        let temp = model.temperature
        self.infoLabel.text = "\(Int(temp))º"
    }
    
    private func configureInfoLabelConstaint() {
        self.view.addSubview(infoLabel)
        infoLabel.translatesAutoresizingMaskIntoConstraints = false
        infoLabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 10).isActive = true
        infoLabel.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -10).isActive = true
        infoLabel.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 200).isActive = true
        infoLabel.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -300).isActive = true
    }
    
}
