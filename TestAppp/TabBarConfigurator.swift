import UIKit


enum TabBarConfigurator: Int, CaseIterable {
    
    case location
    case data
    
    var image: UIImage {
        switch self {
        case .location:
            return UIImage(named: "settings_nonActive")!
        case .data:
            return UIImage(named: "finance_nonActive")!
        }
    }
    
    var selectedImage: UIImage {
        switch self {
        case .location:
            return UIImage(named: "settings_active")!
        case .data:
            return UIImage(named: "finance_active")!
        }
    }
}
