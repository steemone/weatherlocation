import UIKit
import MapKit

class TabBarViewController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        UITabBar.appearance().barTintColor = .systemBackground
        tabBar.tintColor = .label
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupVCs()

    }
    
    
    private func createNavController(for rootViewController: UIViewController,
                                     title: String,
                                     image: UIImage) -> UIViewController {
        let navController = UINavigationController(rootViewController: rootViewController)
        navController.tabBarItem.title = title
        navController.tabBarItem.image = image
        navController.navigationBar.prefersLargeTitles = true
        rootViewController.navigationItem.title = title
        return navController
    }
    
    func setupVCs() {
        viewControllers = [
            createNavController(for: FirstViewController(),
                                title: NSLocalizedString("Location", comment: ""),
                                image: UIImage(systemName: "magnifyingglass")!),
            createNavController(for: SecondViewController(),
                                title: NSLocalizedString("Data", comment: ""),
                                image: UIImage(systemName: "house")!)
        ]
    }
    
}




