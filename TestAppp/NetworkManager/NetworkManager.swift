import Foundation
import CoreLocation

class NetworkManager {
    static let shared = NetworkManager()
    private init() {}
    
    
    func getWeather(latitude: Double, longitude: Double, completion: @escaping (MainWeatherModel?) -> ()) {
        guard let url = URL(string: "https://api.openweathermap.org/data/2.5/onecall?lat=\(latitude)&lon=\(longitude)&units=metric&exclude=minutely&lang=ru&appid=80fa3e23804079f27e8ccd16eb117295") else {
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { [weak self] (data, response, error) in
            if error == nil, let data = data {
                do {
                    let decoderOfferModel = try JSONDecoder().decode(MainWeatherModel.self, from: data)
                    DispatchQueue.main.async {
                        completion(decoderOfferModel)
                    }
                } catch {
                    print(error)
                }
            }
        }
        task.resume()
    }
    
    func sendRequest(coordinates: CLLocationCoordinate2D, completion: @escaping (MainWeatherModel?) -> ()) {
        let latitude = coordinates.latitude
        let longitude = coordinates.longitude
        let query = String(latitude) + "," + String(longitude)
        
        print(query)
        guard let url = URL(string: "https://api.openweathermap.org/data/2.5/onecall?lat=\(latitude)&lon=\(longitude)&units=metric&exclude=minutely&lang=ru&appid=80fa3e23804079f27e8ccd16eb117295") else {
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if error == nil, let data = data {
                do {
                    let decoderOfferModel = try JSONDecoder().decode(MainWeatherModel.self, from: data)
                    DispatchQueue.main.async {
                        completion(decoderOfferModel)
                    }
                } catch {
                    print(error)
                }
            } else {
                print(error?.localizedDescription ?? "error")
            }
        }
        task.resume()
    }
    
    
}
