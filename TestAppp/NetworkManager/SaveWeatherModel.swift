import UIKit

struct SaveWeatherModel: Codable {
    
    let date: Int
    let city: String
    let longitude: Double
    let latitude: Double
    let temperature: Double
    
    
//    init(date: Int?, city: String?, longitude: Double?, latitude: Double?, temperature: Double?) {
//        self.date = date
//        self.city = city
//        self.longitude = longitude
//        self.latitude = latitude
//        self.temperature = temperature
//    }
    
    enum CodingKeys: String, CodingKey {
        case date
        case city
        case longitude
        case latitude
        case temperature
    }
    
//    init(from decoder: Decoder) throws {
//        let container = try decoder.container(keyedBy: CodingKeys.self)
//        date = try container.decodeIfPresent(Int.self, forKey: .date)
//        city = try container.decodeIfPresent(String.self, forKey: .city)
//        longitude = try container.decodeIfPresent(Double.self, forKey: .longtitude)
//        latitude = try container.decodeIfPresent(Double.self, forKey: .latitude)
//        temperature = try container.decodeIfPresent(Double.self, forKey: .temperature)
//    }
//
//    func encode(to encoder: Encoder) throws {
//        var container = encoder.container(keyedBy: CodingKeys.self)
//        try container.encode(self.date, forKey: .date)
//        try container.encode(self.city, forKey: .city)
//        try container.encode(self.longitude, forKey: .longtitude)
//        try container.encode(self.latitude, forKey: .latitude)
//        try container.encode(self.temperature, forKey: .temperature)
//    }
}


extension UserDefaults {
    
    func set<T: Encodable>(encodable: T, forKey key: String) {
        if let data = try? JSONEncoder().encode(encodable) {
            set(data, forKey: key)
        }
    }
    
    func value<T: Decodable>(_ type: T.Type, forKey key: String) -> T? {
        if let data = object(forKey: key) as? Data,
           let value = try? JSONDecoder().decode(type, from: data) {
            return value
        }
        return nil
    }
}


