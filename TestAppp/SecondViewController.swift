import UIKit

class SecondViewController: UIViewController {
    
    struct Cells {
        static let locationCell = "LocationCell"
    }
    
    var weatherArray: [SaveWeatherModel] = []
    
    lazy var tableView: UITableView = {
        let table = UITableView()
        table.backgroundColor = UIColor.rgbColorFor(hex: "EDF3F4")
        table.register(LocationCell.self, forCellReuseIdentifier: Cells.locationCell)
        table.isHidden = false
        table.layer.cornerRadius = 15
        return table
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.backgroundColor = .purple
        configureTableView()
        guard let element = UserDefaults.standard.value([SaveWeatherModel].self, forKey: "Weather") else {return}
        self.weatherArray = element
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        configureConstraintTableView()
    }
    
    
    private func setTableViewDelegates() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    private func configureConstraintTableView() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
    }
    
    private func configureTableView() {
        self.view.addSubview(tableView)
        setTableViewDelegates()
    }
    
}

extension SecondViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.weatherArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Cells.locationCell) as! LocationCell
        cell.configureSetupInfoLabel(model: weatherArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
        let vc = InformationViewController(saveModel: weatherArray[indexPath.row])
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
}
